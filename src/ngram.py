'''
Created on Mar 30, 2013

@author: dirkvangheel
'''
import utilities
import scipy.sparse as sp

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

from data_transform_1 import DataTranformer1
from data_transform_2 import DataTranformer2
from data_transform_4 import DataTranformer4

from sklearn.externals.joblib import Parallel, delayed
from math import modf
import thread


import re
import unicodedata


def textMetricsTest():
    X, Y = utilities.load_test_data()
    X=X[:2]
    
    print 'TEXT:', X[1]['text']
    transformer = DataTranformer4()
    transformer.extendX(X)
    print X[1]
    
def tokenizeWords(txt):
    token_pattern = ur"(?u)\b\w\w+\b"
    token_pattern = re.compile(token_pattern)
    return lambda doc: token_pattern.findall(doc)


def vectorizerTests():
    X, Y = utilities.load_data_by_type('test')
    print X[0]
    X = [r['text'] for r in X]
    print X[0] 
    count_vect = CountVectorizer()
    
    #X = count_vect.fit_transform(X[:100])
    #print count_vect.get_feature_names()
    #print X.shape
    #print X.getrow(0)
        
    vectorizer = TfidfVectorizer()
    X= vectorizer.fit_transform(X[:100])
    #print count_vect.get_feature_names()
    print X.shape
    print X.getrow(0)
    print 'TEST:'
    print vectorizer.transform(['this is a test review. maybe of a restaurant or something. it was very good service.']);
    
   

def dataTransformTests():
    #X = utilities.load_X('test')
    X, Y = utilities.load_training_data()
    print 'input features:'
    for key in X[0]:
        print key
    
    model = DataTranformer2()
    model.fit(X[:100])
    
    print 'model features:'
    print model.get_feature_names()
    
    print 'transformed input:'
    print model.transform(X[:100]).getrow(0)

def runTask1(param):
    res= 1
    for i in range(1000):
        res = res - 1.5
    print 'runTask1(%d)' % param
    return 'runTask1(%d): %d' % (param, res)

def runTask2(param):
    res= 1
    for i in range(1000):
        res = res + 1
    print 'runTask2(%d)' % param
    return 'runTask2(%d): %d' % (param, res)

def dispatch(function, param):
    return function(param)
    
def parallelTest():
    #r = Parallel(n_jobs=2)(delayed(runTask1)(i) for i in range(10))
    r = Parallel(n_jobs=2)([delayed(runTask1)(1), delayed(runTask2)(2), delayed(runTask1)(3), delayed(runTask2)(4)])
    print r
    
def capitalwords():
    doc = "Dit IS een Test? zal het LUKKEN????"
    print doc
    token_pattern = re.compile('[A-Z]{2,}')
    print len(token_pattern.findall(doc))
    for w in token_pattern.findall(doc):
        print w

    
def users_test_missing_in_training():
    X = utilities.load_user('training')
    users = [ u['user_id'] for u in X ]
    print len(users)
    
    testReviews = utilities.load_review('test')
    
    missingUsers = [ r['user_id'] for r in testReviews if r['user_id'] not in users ]
    print 'number of users from test reviews missing in training users: %d' % len(missingUsers)

if __name__ == '__main__':
    #vectorizerTests()
    #dataTransformTests()
    #parallelTest()
    #textMetricsTest()
    #users_test_missing_in_training()
    capitalwords()
    
    
    
    