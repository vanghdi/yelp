'''
Created on Apr 2, 2013

@author: dirkvangheel
'''

from pymongo import MongoClient
import utilities
import logging, log
from gensim import corpora, models, utils
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize, wordpunct_tokenize, sent_tokenize
from gensimLda import getTopicDistribution, tokenize, get_stop_words
import nltk
from nltk.collocations import *
from nltk.util import bigrams
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import numpy as np
import pylab as pl

def load_business(db):
    logging.info('load data')
    x = utilities.load_business_idx()
    logging.info('load documents')
    collection = db.business
    db.business.remove()
    for r in x:
        doc = {}
        doc['_id'] = r
        doc.update(x[r])
        collection.insert(doc)
    logging.info('done')  

def load_user(db):
    collection = db.user
    db.user.remove()
    
    logging.info('load data')
    x = utilities.load_user('training')
    logging.info('load documents')
    for u in x:
        doc = u
        doc['type'] = 'training'
        #doc['_id'] = u['user_id']
        doc.update()
        collection.insert(doc)
    logging.info('done')  
    
    logging.info('load data')
    x = utilities.load_user('test')
    logging.info('load documents')
    for u in x:
        doc = u
        doc['type'] = 'test'
        #doc['_id'] = u['user_id']
        doc.update()
        collection.insert(doc)
    logging.info('done')  
    
    
DICTIONARY_FILE = '../data/yelp/review-1.dict'
CORPUS_FILE = '../data/yelp/review-1.mm'
LDA_FILE = '../data/yelp/review-1.lda'
    
def load_lda(db, name):
    DICTIONARY_FILE = '../data/yelp/review-'+name+'.dict'
    LDA_FILE = '../data/yelp/review-'+name+'.lda'
    
    logging.info('DataTranformer5 - load dictionary (%s)'% DICTIONARY_FILE)
    dictionary = corpora.Dictionary.load(DICTIONARY_FILE)
    logging.info('DataTranformer5 - load LDA Model (%s)'% LDA_FILE)
    lda = models.ldamodel.LdaModel.load(LDA_FILE)
    
    i = 0;
    for r in db.reviewTokens.find():
        i+=1
        tokens = r['tokens']
        if 'ngrams' in r:
            tokens = tokens + r['ngrams']
        doc = dictionary.doc2bow(tokens)
        topics = {}
        for topic in lda[doc]:
            topics['topic_'+str(topic[0])] = topic[1]
        #print r['review_id'], topics
        
        db.review.update({'review_id': r['_id']}, { '$set': {'topics_'+name : topics }})
        if i%2500==0:
            print 'progress -', i

def load_topics(db, name, n_topics):
    LDA_FILE = '../data/yelp/review-'+name+'.lda'            
    lda = models.ldamodel.LdaModel.load(LDA_FILE)
    db.topic.remove({'name':name});
    for i in range(n_topics):
        topic_words = [t[1] for t in lda.show_topic(i, 10)]
        print i, topic_words    
        db.topic.insert({'topic_id': i, 'name':name, 'words': topic_words});



def load_reviews(db):
    collection = db.review
    db.review.remove()
    
    logging.info('load training data')
    x, y= utilities.load_training_data()
    logging.info('load documents')
    for r in x:
        r['type'] = 'training'
        collection.insert(r)
    
    logging.info('load test data')
    x, y= utilities.load_test_data()
    logging.info('load documents')
    for r in x:
        r['type'] = 'test'
        collection.insert(r)
    logging.info('done')
    
def load_review_tokens(db):
    lmtzr= WordNetLemmatizer()
    collection = db.reviewTokens
    collection.remove()
    logging.info('load training data')
    x, y= utilities.load_training_data()
    logging.info('load documents')
    for r in x:
        doc={}
        doc['_id']= r['review_id']
        doc['tokens'] = tokenize(lmtzr, r['text'])
        collection.insert(doc)
    
    logging.info('load test data')
    x, y= utilities.load_test_data()
    logging.info('load documents')
    for r in x:
        doc={}
        doc['_id']= r['review_id']
        doc['tokens'] = tokenize(lmtzr, r['text'])
        collection.insert(doc)
    logging.info('done')
        

def load_ngrams(db):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    logging.info('load all tokens')
    tokens=[]
    n = db.reviewTokens.count()
    i = 0
    db.bigrams.remove()
    db.trigrams.remove()
    step = 10000
    keepGoing = True
    while keepGoing > 0: 
        ii=0
        tokens=[]
        for r in db.reviewTokens.find().limit(step).skip(i):
            ii+=1
            tokens = tokens + r['tokens']
        keepGoing = (ii==step)
        logging.info('number of tokens: %d' % len(tokens))  
        finder = BigramCollocationFinder.from_words(tokens)
        finder.apply_freq_filter(10) 
        for t in finder.nbest(bigram_measures.pmi, 100):
            token=' '.join(t)
            db.bigrams.update({'token':token}, {'token':token}, upsert=True);
        
        finder = TrigramCollocationFinder.from_words(tokens)
        finder.apply_freq_filter(10) 
        for t in finder.nbest(trigram_measures.pmi, 100):
            token=' '.join(t)
            db.trigrams.update({'token':token}, {'token':token}, upsert=True);
        i+= step


def load_ngrams_in_reviewtokens(db):
    # load ngrams
    ngrams=[ t['token'] for t in db.bigrams.find() ]
    for t in db.trigrams.find():
        ngrams.append(t['token'])
    logging.info("%d ngrams loaded"%len(ngrams))
    
    i=0
    for r in db.review.find({},{'review_id':1, 'text':1}):
        i+=1
        if i%10000==0:
            logging.info('loading ngrams - progress %d'%i)
        matches = [ ngram for ngram in ngrams if ngram in r['text']]
        if len(matches)>0:
            db.reviewTokens.update({'_id': r['review_id']},{'$set': {'ngrams': matches}})
            
def buildLdaModel(name, n_topics):
    DICTIONARY_FILE = '../data/yelp/review-'+name+'.dict'
    CORPUS_FILE = '../data/yelp/review-'+name+'.mm'
    LDA_FILE = '../data/yelp/review-'+name+'.lda'
    
    logging.info('buildYelpDictionary - load tokens')
    texts = []
    for txt in db.reviewTokens.find():
        if 'ngrams' in txt:
            texts.append(txt['ngrams'])
        texts.append(txt['tokens'])
        
    logging.info('buildYelpDictionary - build dictionary')
    dictionary = corpora.Dictionary(texts)
    stoplist = get_stop_words()
    logging.info('buildYelpDictionary - check frequencies')
    once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.iteritems() if docfreq == 1]
    stopwords_ids = [dictionary.token2id[stopword] for stopword in stoplist if stopword in dictionary.token2id]
    logging.info('buildYelpDictionary - tokens only occurring once: %d' % len(once_ids))
    logging.info('buildYelpDictionary - filter tokens')
    dictionary.filter_tokens(once_ids + stopwords_ids) # remove stop words and words that appear only once    
    logging.info('buildYelpDictionary - compactify')
    dictionary.compactify() # remove gaps in id sequence after words that were removed
    logging.info('buildYelpDictionary - save dictionary')
    dictionary.save(DICTIONARY_FILE)
    
    logging.info('buildYelpDictionary - build corpus')
    corpus = [dictionary.doc2bow(text) for text in texts]
    logging.info('buildYelpDictionary - save corpus')
    corpora.MmCorpus.serialize(CORPUS_FILE, corpus) 
    
    logging.info('buildLdaModel - build LDA Model')
    lda = models.ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=n_topics)
    logging.info('buildLdaModel - save model')
    lda.save(LDA_FILE);
    logging.info('buildLdaModel - done')  
    
    
def load_text_analytics(db):
    for r in db.review.find():
        if len(r['text'])>0:
            sentences = sent_tokenize(r['text'])
            words = [wordpunct_tokenize(s) for s in sentences ]
            wordLen = [[len(w) for w in s] for s in words if len(s)>0]
            wordLen = [i for n in wordLen for i in n ]
            sentLen = [len(s)*1.0 for s in words ]
            
            avgWordLen = [sum([len(w) for w in s])*1.0/len(s) for s in words if len(s)>0 ]
            metrics={}
            metrics['n_sentences']= len(sentences)
            metrics['avg_word_len']= sum(avgWordLen)/len(sentences)
            metrics['avg_sent_len']= sum(sentLen)/len(sentLen)
            metrics['perc_sentence_5']= len([n for n in sentLen if n <=5])*1.0/len(sentLen)
            metrics['perc_sentence_7']= len([n for n in sentLen if 5<n and n <=7])*1.0/len(sentLen)
            metrics['perc_sentence_10']= len([n for n in sentLen if 7<n and n <=10])*1.0/len(sentLen)
            metrics['perc_sentence_12']= len([n for n in sentLen if 10<n and n <=12])*1.0/len(sentLen)
            metrics['perc_sentence_15']= len([n for n in sentLen if 12<n and n <=15])*1.0/len(sentLen)
            metrics['perc_sentence_above_15']= len([n for n in sentLen if n>15])*1.0/len(sentLen)
            metrics['perc_word_5']=len([n for n in wordLen if n<=5])*1.0/len(wordLen)
            metrics['perc_word_7']=len([n for n in wordLen if 5<n and n<=7])*1.0/len(wordLen)
            metrics['perc_word_10']=len([n for n in wordLen if 7<n and n<=10])*1.0/len(wordLen)
            metrics['perc_word_12']=len([n for n in wordLen if 10<n and n<=12])*1.0/len(wordLen)
            metrics['perc_word_15']=len([n for n in wordLen if 12<n and n<=15])*1.0/len(wordLen)
            metrics['perc_word_above_15']=len([n for n in wordLen if n>15])*1.0/len(wordLen)
            db.review.update({'_id':r['_id']}, {'$set': metrics})

def user_info():
    x=[]    
    for item in db.review.find({'user_votes_useful':{'$exists':True}},
                               {'votes.useful':1, 'user_votes_useful':1, 'user_review_count':1, '_id':0}):
        x.append([item['votes']['useful'], item['user_votes_useful'], item['user_review_count']])

    x = np.array(x)
    x = np.sort(x, axis=0)
    print x.shape
    pl.subplot(3,1,1) # n_rows, n_cols, plot number
    pl.scatter(x[:,0], x[:,1])
    pl.xlabel('review useful votes')
    pl.ylabel('user useful votes')
    #pl.title('relationship useful review votes vs user useful votes')
    
    pl.subplot(3,1,2)
    pl.scatter(x[:,0], x[:,2])
    pl.xlabel('review useful votes')
    pl.ylabel('user review count')
    #pl.title('relationship useful review votes vs user review count')
    
    pl.subplot(3,1,3)
    pl.scatter(x[:,1], x[:,2])
    pl.xlabel('user useful votes')
    pl.ylabel('user review count')
    #pl.title('relationship user useful votes vs user review count')
    
    
    pl.show()

def load_tfidf(db):
    X=[]
    for r in db.review.find():
        x = r['text']
        X.append(x)
    vect = TfidfVectorizer()
    vect.fit(X)
    utilities.save_pickle('tfidf', vect)

if __name__=="__main__":
    client = MongoClient('localhost', 27017)
    db = client.yelp
    #load_user(db)
    #load_topics(db,'ngrams', 200)
    #load_review_tokens(db)
    #load_ngrams(db)
    #load_ngrams_in_reviewtokens(db)
    #buildLdaModel('ngrams', 200)
    #load_reviews(db)
    #load_lda(db, 'ngrams')
    #load_text_analytics(db)
    #load_tfidf(db)
    user_info()