
conn = new Mongo();
db = conn.getDB("yelp");

var res = db.user.aggregate(
    { '$match': { 'votes': { '$exists': true } } },
    { '$project': {	'useful' : '$votes.useful',
    				'funny' : '$votes.funny',
    				'cool' : '$votes.cool',
    				'review_count' : '$review_count'
    		}
    },
    //{ '$limit': 10},
    { '$group' : { '_id' : 1,
    		'avg_useful' : { '$avg' : '$useful'},
    		'avg_funny' : { '$avg' : '$funny'},
    		'avg_cool' : { '$avg' : '$cool'},
    		'avg_review_count' : { '$avg' : '$review_count' },
    		'min_useful' : { '$min' : '$useful'},
    		'max_useful' : { '$max' : '$useful'},
    		'n' : { '$sum' : 1 }
    	}
    }
    
);

printjson(res);
