
conn = new Mongo();
db = conn.getDB("yelp");

var res = db.review.aggregate(
    { '$match': { 'type': 'training' } },
    { '$project': {	'useful' : '$votes.useful',
    				'funny' : '$votes.funny',
    				'cool' : '$votes.cool'    				
    		}
    },
    //{ '$limit': 10},
    { '$group' : { '_id' : 1,
    		'avg_useful' : { '$avg' : '$useful'},
    		'avg_funny' : { '$avg' : '$funny'},
    		'avg_cool' : { '$avg' : '$cool'},
    		'n' : { '$sum' : 1 }
    	}
    }
    
);

printjson(res);
