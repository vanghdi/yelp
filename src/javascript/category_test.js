
conn = new Mongo();
db = conn.getDB("yelp");

var res = db.review.aggregate(
	//{ '$limit': 1},
	{ '$unwind': '$bus_categories' },
	{ '$group' : { '_id' : {'type':'$type', 'category':'$bus_categories'},
				   'avg_age' : {$avg: '$age'},
				   'avg_useful': {$avg: '$votes.useful'},
				   'max_useful': {$max: '$votes.useful'},
				   'avg_funny': {$avg: '$votes.funny'},
				   'avg_cool': {$avg: '$votes.cool'}
		} 
	}
);

printjson(res);
