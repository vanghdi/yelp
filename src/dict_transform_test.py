'''
Created on Nov 9, 2012

@author: vanghdi
'''


from dict_transform import DictVectorizer


if __name__ == '__main__':
    input = [{'x':'x1','y':'y1'},
             {'x':'x2','y':['y2-1','y2-2']},
             {         'y':'y3'}]

    vect = DictVectorizer()
    
    vect.fit(input)
    
    print vect.get_feature_names()
    print vect.transform(input)
    
    test = [ {'x':'x1','y':'y1','a':'a1'},
             {'x':['x1','x2'],'y':['y2-1','y2-3']},
             {         'y':'y3'}]
    
    print vect.get_feature_names()
    print vect.transform(test)
    
    
