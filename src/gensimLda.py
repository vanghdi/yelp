'''
Created on Apr 6, 2013

@author: dirkvangheel
'''

from gensim import corpora, models, similarities, utils
import nltk
import string
from nltk.stem.wordnet import WordNetLemmatizer
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS

import logging
import utilities
 



def tokenize(lmtzr, text, bigram=False):
    doc = [ lmtzr.lemmatize(w) for w in utils.tokenize(text, deacc=True, errors='strict', to_lower=True) ]
    if bigram:
        doc.append([' '.join(bi) for bi in nltk.util.bigrams(doc)])
    return doc


def get_stop_words():
    stoplist = []
    for w in ENGLISH_STOP_WORDS:
        stoplist.append(w)
    
    for w in nltk.corpus.stopwords.words('english'):
        if w not in stoplist:
            stoplist.append(w)
    
    return stoplist

def buildYelpDictionaryAndCorpus(bigram=False):
    X = utilities.load_review('training')
    #X = X[:10000]
    lmtzr = WordNetLemmatizer()
    
    logging.info('buildYelpDictionary - tokenize text')
    stoplist = get_stop_words()    
    texts = []
    i = 0
    for x in X:
        i += 1
        texts.append(tokenize(lmtzr, x['text']))
        if i % 5000 == 0 :
            logging.info('buildYelpDictionary - progress: %d documents tokenized' % i)
    
    # build dictionary
    logging.info('buildYelpDictionary - build dictionary')
    dictionary = corpora.Dictionary(texts)
    
    logging.info('buildYelpDictionary - check frequencies')
    once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.iteritems() if docfreq == 1]
    stopwords_ids = [dictionary.token2id[stopword] for stopword in stoplist if stopword in dictionary.token2id]
    logging.info('buildYelpDictionary - tokens only occurring once: %d' % len(once_ids))
    logging.info('buildYelpDictionary - filter tokens')
    dictionary.filter_tokens(once_ids + stopwords_ids) # remove stop words and words that appear only once    
    logging.info('buildYelpDictionary - compactify')
    dictionary.compactify() # remove gaps in id sequence after words that were removed
    logging.info('buildYelpDictionary - save dictionary')
    dictionary.save(DICTIONARY_FILE)
    
    logging.info('buildYelpDictionary - build corpus')
    corpus = [dictionary.doc2bow(text) for text in texts]
    logging.info('buildYelpDictionary - save corpus')
    corpora.MmCorpus.serialize(CORPUS_FILE, corpus) 
    logging.info('buildYelpDictionary - done')
    
def buildLdaModel(n_topics=100):
    logging.info('buildLdaModel - load dictionary')
    dictionary = corpora.Dictionary.load(DICTIONARY_FILE)
    logging.info('buildLdaModel - load corpus')
    corpus = corpora.MmCorpus(CORPUS_FILE)
    
    logging.info('buildLdaModel - build LDA Model')
    lda = models.ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=n_topics)
    logging.info('buildLdaModel - save model')
    lda.save(LDA_FILE);
    logging.info('buildLdaModel - done')
    
    
def getTopicDistribution(dictionary, lda, lmtzr, text):
    tokens = tokenize(lmtzr, text)
    doc = dictionary.doc2bow(tokens)
    print tokens
    topics = lda[doc]
    print topics
    
    for (i, p) in sorted(topics, key=lambda tup: tup[1], reverse=True):
        print "%f - %s" % (p, lda.show_topic(i))

def testTopics():
    lda = models.ldamodel.LdaModel.load(LDA_FILE)
    dictionary = corpora.Dictionary.load(DICTIONARY_FILE)
    X = utilities.load_review('test')
    lmtzr = WordNetLemmatizer()
    
    getTopicDistribution(dictionary, lda, lmtzr, X[0]['text']) 
    
    getTopicDistribution(dictionary, lda, lmtzr, X[1]['text']) 
    

def bigramTest():
    lmtzr = WordNetLemmatizer()
    tokens = tokenize(lmtzr, 'water, was, what, this is a test.')
    print [' '.join(bi) for bi in nltk.util.bigrams(tokens)]
    

DICTIONARY_FILE = '../data/yelp/review-bigram-1.dict'
CORPUS_FILE = '../data/yelp/review-bigram-1.mm'
LDA_FILE = '../data/yelp/review-bigram-1.lda'
    
if __name__ == '__main__':
    buildYelpDictionaryAndCorpus(bigram=True)
    #buildLdaModel(n_topics=250)   
    #testTopics()
    bigramTest()
    
    