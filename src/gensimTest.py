'''
Gensim Tutorial


Created on Apr 6, 2013

@author: dirkvangheel
'''
import gensim
from gensim import corpora, similarities
import logging
import utilities
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS 

def buildYelpCorpus():
    X,Y = utilities.load_training_data()
    print ENGLISH_STOP_WORDS
    stoplist = ENGLISH_STOP_WORDS
    # TODO: apply proper preprocessing and tokenizing (see data_transform_4
    texts = [[word for word in x['text'].lower().split() if word not in stoplist]
             for x in X]
    
    # remove words that appear only once
    all_tokens = sum(texts, [])
    tokens_once = set(word for word in set(all_tokens) if all_tokens.count(word) == 1)
    texts = [[word for word in text if word not in tokens_once]
             for text in texts]
    
    # build dictionary
    dictionary = corpora.Dictionary(texts)
    dictionary.save('../data/yelp/review.dict')
    
    # build (bag of words) corpus
    corpus = [dictionary.doc2bow(text) for text in texts]
    corpora.MmCorpus.serialize('../data/yelp/review.mm', corpus) # store to disk, for later use
    

def gensimTest():
    documents = ["Human machine interface for lab abc computer applications",
                  "A survey of user opinion of computer system response time",
                  "The EPS user interface management system",
                  "System and human system engineering testing of EPS",
                  "Relation of user perceived response time to error measurement",
                  "The generation of random binary unordered trees",
                  "The intersection graph of paths in trees",
                  "Graph minors IV Widths of trees and well quasi ordering",
                  "Graph minors A survey"]
    
    # remove common words and tokenize
    stoplist = set('for a of the and to in'.split())
    
    texts = [[word for word in document.lower().split() if word not in stoplist]
             for document in documents]
    
    # remove words that appear only once
    all_tokens = sum(texts, [])
    tokens_once = set(word for word in set(all_tokens) if all_tokens.count(word) == 1)
    texts = [[word for word in text if word not in tokens_once]
             for text in texts]
    
    print texts
    
    dictionary = corpora.Dictionary(texts)
    dictionary.save('../data/tmp/deerwester.dict') # store the dictionary, for future reference
    print dictionary
    print dictionary.token2id
    
    print 'New Documents'
    new_doc = "Human computer interaction"
    new_vec = dictionary.doc2bow(new_doc.lower().split())
    print new_vec
    
    corpus = [dictionary.doc2bow(text) for text in texts]
    corpora.MmCorpus.serialize('../data/tmp/deerwester.mm', corpus) # store to disk, for later use
    print 'corpus:', corpus
    
    print 'lda'
    model = gensim.models.ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=100)
    print 'show_topics:'
    model.show_topics()
    print 'topic probability for: ', new_doc
    print model[new_vec] 
    
    '''print 'hdp' 
    hdp_model = models.hdpmodel.HdpModel(corpus, id2word=dictionary)
    print 'hdp - print_topics'
    hdp_model.print_topics(topics=20, topn=10)
    print 'topic probability for: ', new_doc
    print hdp_model[new_vec] # get topic probability distribution for a document
    '''
    
    
def buildDictionaryOnline():
    # collect statistics about all tokens
    dictionary = corpora.Dictionary(line.lower().split() for line in open('../data/tmp/mycorpus.txt'))
    # remove stop words and words that appear only once
    stoplist = set('for a of the and to in'.split())
    stop_ids = [dictionary.token2id[stopword] for stopword in stoplist
                if stopword in dictionary.token2id]
    once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.iteritems() if docfreq == 1]
    dictionary.filter_tokens(stop_ids + once_ids) # remove stop words and words that appear only once
    dictionary.compactify() # remove gaps in id sequence after words that were removed
    print dictionary
    
    
def lda():
    dictionary = corpora.Dictionary.load('../data/yelp/review-1.dict')
    
    print dictionary
    print dictionary.token2id['is']
    

    #model = models.ldamodel.LdaModel(bow_corpus, id2word=dictionary, num_topics=100)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    #gensimTest()
    #buildYelpCorpus()
    #buildDictionaryOnline()
    lda()