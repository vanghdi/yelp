'''
Created on Nov 4, 2012

@author: vanghdi
'''
import logging, log
import utilities

import numpy as np
from math import sqrt

from pymongo import MongoClient

from model_definitions import ModelDefinitions
from sklearn import cross_validation
from sklearn import metrics
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer

from dict_transform import DictVectorizer
from sklearn.preprocessing import StandardScaler

class TxtModel():
    def __init__(self, target):
        # define model`
        self._pipeline = Pipeline([
            ('vect', TfidfVectorizer(ngram_range=(1,3))),            
            ('classifier', MultinomialNB()),
        ]) 
        
        
        self._target = [target]
        self._classifierName='MultinomialNB-'+self._target[0]
        self._transformationName='T0'
        logging.info('target: '+ self._target[0])
        
        logging.info('pipeline loaded:')
        for step in self._pipeline.named_steps:
            logging.info(' -> %s' % step)
    
    def mongo_query(self, featureList):
        columns={}
        columns['_id']=0
        columns['review_id']=1
        for f in featureList:
            columns[f]=1
        return columns
    
    @staticmethod
    def normalise(record, target):
        result = {}
        targetValue=None
        for v in record:
            if isinstance(record[v], dict):
                for vv in record[v]:
                    col=v+'.'+vv
                    result[col]=record[v][vv]
            else:
                col=v
                result[col]=record[v]
            if col in target:
                targetValue=result[col]            
                result.pop(col, None)
        result.pop('review_id',None)
        return result, targetValue
    
    def targetFunction(self, targetValue):
        if targetValue > 0:
            return 1
        else:
            return 0
    
    def getName(self):
        return self._classifierName+'-'+self._transformationName
    
    def get_data(self, db, type):
        X = []
        Y = []
        
        for r in db.review.find({'type': type}, {'votes.useful':1,'text':1}):        
            x, y = TxtModel.normalise(r, self._target)
            X.append(x['text'])
            Y.append(self.targetFunction(y))
        
        return X, Y

    def train(self, db):
        logging.info('train - load data')
        X, Y = self.get_data(db, 'training')            
        logging.info('train - fit model')
        self._pipeline.fit(X, Y)   
        
    def save(self):
        utilities.save_pickle('../models/'+self.getName()+'.pkl', self)     
        
    def cross_validation(self,db):
        logging.info('cross validation - load data')
        X, Y = self.get_data(db, 'training')
        #X = X[:5000]; print 'debug: reduce training data'
        #Y = Y[:5000]
        
        logging.info('cross validation - transform input data (%s)' % self._transformationName)
        X = TfidfVectorizer().fit_transform(X)
        logging.info('cross validation - start cross validation run')
        auc = cross_validation.cross_val_score(MultinomialNB(), 
                                         X, np.array(Y), 
                                         cv=5, 
                                         score_func=metrics.auc_score)
        logging.info('cross validation - done')
        print "auc: %0.2f (+/- %0.2f)" % (auc.mean(), auc.std() / 2)
        
    


if __name__=='__main__':
    client = MongoClient('localhost', 27017)        
    db = client.yelp
    model = TxtModel('votes.useful')
    model.train(db)
    model.save()
  
    model.cross_validation(db)
    
    