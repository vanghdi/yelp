'''
Created on Mar 30, 2013

@author: dirkvangheel
'''
from sklearn.pipeline import Pipeline

# Transformations
from dict_transform import DictVectorizer
from data_transform_0 import DataTranformer0
from data_transform_1 import DataTranformer1
from data_transform_2 import DataTranformer2
from data_transform_3 import DataTranformer3
from data_transform_4 import DataTranformer4
from data_transform_5 import DataTranformer5
from data_transform_6 import DataTranformer6
from data_transform_7 import DataTranformer7
from data_transform_8 import DataTranformer8
from data_transform_9 import DataTranformer9

from sklearn.preprocessing import StandardScaler

# Classifiers
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor



class ModelDefinitions(object):

    def __init__(self):
        pass
    
    def getClassifier(self, classifierName):
        if classifierName == 'linearRegression':
            return LinearRegression() 
        elif classifierName == 'linearSVR':
            return SVR(kernel='linear', max_iter=75000, tol=5)
        elif classifierName == 'rbfSVR':
            return SVR(kernel='rbf', max_iter=150000)
        elif classifierName == 'SGD':
            return SGDRegressor(alpha=0.1)
        elif classifierName == 'randomForrest':
            return RandomForestRegressor(n_estimators=100) 
        elif classifierName == 'gradientBoost':
            #params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 1, 'learning_rate': 0.01, 'loss': 'ls'}
            params = {'n_estimators': 750, 'max_depth': 6, 'min_samples_split': 1, 'learning_rate': 0.01, 'loss': 'ls'}
            return GradientBoostingRegressor(**params)
        
    def supportsSparseInput(self, classifierName):
        if classifierName in  ['randomForrest']:
            return False
        else:
            return True
    
    def getScaler(self):
        return StandardScaler(with_mean=True)
        
    def getTransformation(self, transformationName):
        if transformationName == 'T0':
            transformer = DataTranformer0()
        elif transformationName == 'T1':
            transformer = DataTranformer1()
        elif transformationName == 'T2':
            transformer = DataTranformer2()
        elif transformationName == 'T3':
            transformer = DataTranformer3()
        elif transformationName == 'T4':
            transformer = DataTranformer4(sparse=False)
        elif transformationName == 'T5':
            transformer = DataTranformer5()
        elif transformationName == 'T6':
            transformer = DataTranformer6()
        elif transformationName == 'T7':
            transformer = DataTranformer7(sparse=False)
        elif transformationName == 'T8':
            transformer = DataTranformer8(sparse=False)
        elif transformationName == 'T9':
            transformer = DataTranformer9(sparse=False)
        else:
            raise ValueError('Unknown Transformation: %s' % transformationName)
            
        return transformer

    def getPipeline(self, classifierName, transformationName):
        transformer = self.getTransformation(transformationName)
        scaler = self.getScaler()
        classifier = self.getClassifier(classifierName)
            
        pipeline = Pipeline([
            (transformationName, transformer),
            ('scaler', scaler),
            (classifierName, classifier),
        ])            
        return pipeline