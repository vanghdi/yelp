'''
Created on Nov 4, 2012

@author: vanghdi
'''
import logging, log
import utilities

import numpy as np
from math import sqrt

from pymongo import MongoClient

from sklearn import cross_validation
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from dict_transform import DictVectorizer
from sklearn.preprocessing import StandardScaler

from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor


class UserUsefulModel():
    
    
    def __init__(self, classifier, target):
        # define model`
        self._scaler=StandardScaler(with_mean=True)
        self._pipeline = Pipeline([
            ('vect', DictVectorizer(sparse=False)),
            ('scaler', self._scaler),
            ('classifier', self.getClassifier(classifier)),
        ]) 
        
        textMetrics=['avg_sent_len','avg_word_len','n_sentences','perc_sentence_5','perc_sentence_7','perc_sentence_10','perc_sentence_12',
                     'perc_sentence_15','perc_sentence_above_15','perc_word_5','perc_word_7','perc_word_10','perc_word_12',
                     'perc_word_15','perc_word_above_15']
        userMetrics=['user_average_stars','user_review_count']
        topicMetrics=[]
        for i in range(200):
            topicMetrics.append('topics_ngrams.topic_'+str(i))
        self._target = [target]
        self._features = ['stars']+ topicMetrics + textMetrics + userMetrics + self._target
        #self._features = ['stars' ] + self._target
        self._classifierName=classifier
        self._transformationName=self._target[0]
        logging.info('target: '+ self._target[0])
        

        logging.info('pipeline loaded:')
        for step in self._pipeline.named_steps:
            logging.info(' -> %s' % step)
    
    def mongo_query(self, featureList):
        columns={}
        columns['_id']=0
        #columns['review_id']=1
        for f in featureList:
            columns[f]=1
        return columns
    
    @staticmethod
    def normalise(record, target):
        result = {}
        targetValue=None
        for v in record:
            col=None
            if isinstance(record[v], dict):
                for vv in record[v]:
                    col=v+'.'+vv
                    result[col]=record[v][vv]
            else:
                col=v
                result[col]=record[v]
            if col in target:
                targetValue=result[col]            
                result.pop(col, None)
        #result.pop('review_id',None)
        return result, targetValue
    
    
    
    def getName(self):
        return self._classifierName+'-'+self._transformationName
    
    def get_data(self, db, type):
        X = []
        Y = []
        for r in db.review.find({'type': type, self._target[0]: {'$exists':True}}, self.mongo_query(self._features)):        
            x, y = UserUsefulModel.normalise(r, self._target)
            X.append(x)
            Y.append(y)
        
        return X, Y
    
    def get_data_by_review_id(self, db, review_id):
        X = []
        Y = []
        for r in db.review.find({'review_id': review_id}, self.mongo_query(self._features)):        
            x, y = UserUsefulModel.normalise(r, self._target)
            X.append(x)
            Y.append(y)
        
        return X, Y
    
    def predict(self, db, review_id):
        X,Y = self.get_data_by_review_id(db, review_id)
        return self._pipeline.predict(X)

    def train(self, db):
        logging.info('train - load data')
        X, Y = self.get_data(db, 'training') 
        print X[:1]
        print Y[:1]
        logging.info('train - fit model')
        self._pipeline.fit(X, Y)   
        
    def save(self):
        utilities.save_pickle('../models/'+self.getName()+'.pkl', self)     
        
    def cross_validation(self,db):
        logging.info('cross validation - load data')
        X, Y = self.get_data(db, 'training')
        #X = X[:5000]; print 'debug: reduce training data'
        #Y = Y[:5000]
        logging.info('cross validation - transform input data (%s)' % self._transformationName)
        X = DictVectorizer(sparse=False).fit_transform(X)
        logging.info('cross validation - scale input data')
        X = self._scaler.fit_transform(X)
        logging.info('cross validation - start cross validation run')
        mse = cross_validation.cross_val_score(self.getClassifier(self._classifierName), 
                                         X, np.array(Y), 
                                         cv=5, 
                                         score_func=metrics.mean_squared_error)
        logging.info('cross validation - done')
        rmse = np.sqrt(mse)
        print "mse: %0.2f (+/- %0.2f)" % (mse.mean(), mse.std() / 2)
        print "rmse: %0.2f (+/- %0.2f)" % (rmse.mean(), rmse.std() / 2)

    
    def getClassifier(self, classifierName):
        if classifierName == 'linearRegression':
            return LinearRegression() 
        elif classifierName == 'linearSVR':
            return SVR(kernel='linear', max_iter=75000, tol=5)
        elif classifierName == 'rbfSVR':
            return SVR(kernel='rbf', max_iter=150000)
        elif classifierName == 'SGD':
            return SGDRegressor(alpha=0.1)
        elif classifierName == 'randomForrest':
            return RandomForestRegressor(n_estimators=100) 
        elif classifierName == 'gradientBoost':
            params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 1, 'learning_rate': 0.01, 'loss': 'ls'}
            return GradientBoostingRegressor(**params)
  

if __name__=='__main__':
    client = MongoClient('localhost', 27017)        
    db = client.yelp
    #model = UserUsefulModel('randomForrest', 'user_votes_useful')
    #model = UserUsefulModel('randomForrest', 'user_votes_cool')
    model = UserUsefulModel('randomForrest', 'user_votes_funny')
    model.train(db)
    model.save()
  
    model.cross_validation(db)
    
    