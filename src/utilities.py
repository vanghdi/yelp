'''
Created on Nov 4, 2012

@author: vanghdi
'''
import json
import logging, log
import datetime
import pickle

def read_data(fileName):
    file = open(fileName,'r')
    logging.info("reading file '%s'"%fileName)
    data = [];
    for line in file:
        try:
            # fixed in new data set
            '''if 'test' in fileName and 'user' in fileName:
                # data quality issue: replace only necessary for test-user file (uses single instead of double quotes
                line = line.replace("'", '"')'''
            data.append(json.loads(line)); 
        except ValueError:
            print 'Error', line
    logging.info('file loaded (%d rows)'%len(data)) 
    file.close()
    return data

def load_business(type):
    if type=='training':
        return read_data('../data/yelp_training_set/yelp_training_set_business.json');
    else:
        return read_data('../data/yelp_test_set/yelp_test_set_business.json');

def load_review(type):
    if type=='training':
        return read_data('../data/yelp_training_set/yelp_training_set_review.json');
    else:
        return read_data('../data/yelp_test_set/yelp_test_set_review.json');  

def load_checkin(type):
    if type=='training':
        return read_data('../data/yelp_training_set/yelp_training_set_checkin.json');
    else:
        return read_data('../data/yelp_test_set/yelp_test_set_checkin.json');

def load_user(type):
    if type=='training':
        return read_data('../data/yelp_training_set/yelp_training_set_user.json');  
    else:
        return read_data('../data/yelp_test_set/yelp_test_set_user.json');

def load_business_idx():
    res ={}
    for business in load_business('training') + load_business('test'):
        id= business['business_id']
        entry={}
        for key in business:
            entry['bus_'+key]=business[key]        
        res[id] = entry;
    return res


def updateCount(map, value, key):
    if key not in map:
        map[key] = value
    else:
        map[key] += value

def load_checkin_idx():
    res ={}
    for row in load_checkin('training')+load_checkin('test'):
        id= row['business_id']
        entry={}
        
        valuesByDay={}
        valuesByHour={}
        for key in row['checkin_info']:
            #entry['bus_'+key]=business[key]
            value = row['checkin_info'][key]
            hourOfDay = 'checkin-time-'+key.split('-')[0]
            dayOfWeek = 'checkin-day-'+key.split('-')[1] 
            updateCount(valuesByHour, value, hourOfDay)
            updateCount(valuesByDay, value, dayOfWeek)
        entry.update(valuesByHour)
        entry.update(valuesByDay)            
        res[id] = entry;
    return res  

def load_user_idx():
    res ={}
    for user in load_user('training') + load_user('test'):
        id= user['user_id']
        entry={}
        # normalize votes 
        #entry['user_votes_useful']= user['votes']['useful']
        #entry['user_votes_fun']= user['votes']['funny']
        #entry['user_votes_cool']= user['votes']['cool']
        for key in user:
            # rename
            entry['user_'+key]=user[key]        
        res[id] = entry;
    return res     

def transform_review(review, type):
    if type == 'training':
        t0 = datetime.datetime.strptime('2013-01-19',"%Y-%m-%d").date()
    else:
        t0 = datetime.datetime.strptime('2013-03-12',"%Y-%m-%d").date()
        
    review['age'] = (t0 - datetime.datetime.strptime(review['date'], "%Y-%m-%d").date()).days
    
    return review

def add_user_data(entry, userIdx, user_id):
    try:
        entry.update(userIdx[user_id])
    except KeyError:
        #logging.error("Missing User data for id: %s"%user_id)
        x=1;


def add_business_data(entry, businessIdx, business_id):
    try:
        entry.update(businessIdx[business_id])
    except KeyError:
        #logging.error("Missing Business data for id: %s"%business_id)
        x=2;

def add_checkin_data(entry, checkinIdx, business_id):
    try:
        entry.update(checkinIdx[business_id])
    except KeyError:
        #logging.error("Missing Business data for id: %s"%business_id)
        x=2;
 
def load_topics_idx():
    from pymongo import MongoClient
    client = MongoClient('localhost', 27017)
    db = client.yelp
    idx={}
    for r in db.review.find({},{'topics_ngrams':1, 'review_id':1}):
        idx[r['review_id']] = r['topics_ngrams']
    return idx
    
# normalized data set (normalized: single dictionary list)
def load_data_by_type(type):
    review = load_review(type)
    businessIdx = load_business_idx()
    userIdx = load_user_idx()
    checkinIdx = load_checkin_idx()
    topicsIdx = load_topics_idx()
    
    X=[]
    Y=[]
    for r in review:
        entry=transform_review(r, type)
        # add business data
        add_business_data(entry, businessIdx, r['business_id'])
        # add user data
        add_user_data(entry, userIdx, r['user_id'])
        # add checkin data
        add_checkin_data(entry, checkinIdx, r['business_id'])
        # add topics
        if r['review_id'] in topicsIdx:
            entry.update(topicsIdx[r['review_id']])
        
        X.append(entry)
        if type=='training': 
            Y.append(r['votes']['useful'])
    return X, Y

def load_training_data():
    return load_data_by_type('training')

def load_test_data():
    return load_data_by_type('test') 
 
 
def create_submission(name, ID, Y):
    submissionFile=open('../result/'+name+'.csv','w')
    submissionFile.write('id,votes\n');
    for i in range(len(ID)):
        if Y[i] <0:
            Y[i]=0
        submissionFile.write("%s,%d"%(ID[i],Y[i])+'\n')
    submissionFile.close() 
      
       
def save_pickle(filename, object):
    output = open(filename, 'wb')
    pickle.dump(object, output)
    output.close()        

def read_pickle(filename):
    pkl_file = open(filename, 'rb')
    object = pickle.load(pkl_file)
    
    pkl_file.close()
    return object

        
if __name__=='__main__':
    X, Y= load_training_data()
    
    
    #checkinIdx = load_checkin_idx('test')
    #print checkinIdx['vSC7ncSsitgSpKpMYbzi0w']
    