'''
Created on Nov 4, 2012

@author: vanghdi
'''
import logging, log
import utilities

import numpy as np
import pylab as pl
from math import sqrt

from model_definitions import ModelDefinitions
from sklearn import cross_validation
from sklearn import metrics
from userModels import UserUsefulModel

class RegressionModel():
    def __init__(self, classifierName, transformationName):
        # define model`
        self._classifierName = classifierName
        self._transformationName = transformationName
        
        self._pipeline = ModelDefinitions().getPipeline(classifierName, transformationName)
        
        logging.info('pipeline loaded:')
        for step in self._pipeline.named_steps:
            logging.info(' -> %s' % step)
    
    def getName(self):
        return self._classifierName+'-'+self._transformationName
    
    def train(self):
        logging.info('train - load data')
        X,Y = utilities.load_training_data()
        logging.info('train - fit model')
        self._pipeline.fit(X, Y)   
        logging.info('train - done')
        
    def save(self):
        utilities.save_pickle('../models/'+self.getName()+'.pkl', self)     
        
    def cross_validation(self):
        logging.info('cross validation - load data')
        X,Y = utilities.load_training_data()
        #X = X[:5000]; print 'debug: reduce training data'
        #Y = Y[:5000]
        logging.info('cross validation - transform input data (%s)' % self._transformationName)
        X = ModelDefinitions().getTransformation(self._transformationName).fit_transform(X)
        logging.info('cross validation - scale input data')
        X = ModelDefinitions().getScaler().fit_transform(X)
        logging.info('cross validation - start cross validation run')
        mse = cross_validation.cross_val_score(ModelDefinitions().getClassifier(self._classifierName), 
                                         X, np.array(Y), 
                                         cv=5, 
                                         score_func=metrics.mean_squared_error)
        logging.info('cross validation - done')
        rmse = np.sqrt(mse)
        print "mse: %0.2f (+/- %0.2f)" % (mse.mean(), mse.std() / 2)
        print "rmse: %0.2f (+/- %0.2f)" % (rmse.mean(), rmse.std() / 2)
    
    def create_submission(self):
        logging.info('submission - load test data')
        ID = [r['review_id'] for r in utilities.load_review('test')]
        X, Y= utilities.load_test_data()
        logging.info('submission - predict')
        Y= self._pipeline.predict(X);
        logging.info('submission - write file')
        utilities.create_submission(self.getName(), ID, Y)
        logging.info('submission - done')

def submission_t4_t6():
    linRegT4 = utilities.read_pickle('../models/linearRegression-T4.pkl');
    linRegT6 = utilities.read_pickle('../models/linearRegression-T6.pkl');
    
    logging.info('submission - load test data')
    ID = [r['review_id'] for r in utilities.load_review('test')]
    X, Y= utilities.load_test_data()
    
    i=0;
    Xt4 = [];IDt4 = [];
    Xt6 = [];IDt6 = [];
    for i in range(len(X)):
        if 'user_votes_useful' in X[i]:
            Xt6.append(X[i])
            IDt6.append(ID[i])
        else:
            Xt4.append(X[i])
            IDt4.append(ID[i])  
    logging.info('data loaded:')
    logging.info('T4 - Xt4:'+str(len(Xt4))+' IDt4:'+str(len(IDt4)))
    logging.info('T6 - Xt6:'+str(len(Xt6))+' IDt6:'+str(len(IDt6)))
    logging.info('submission - predict')
    Yt4= linRegT4._pipeline.predict(Xt4);
    Yt6= linRegT6._pipeline.predict(Xt6);
    
    logging.info('submission - write file')
    
    Yt4 = np.hstack((Yt4, Yt6))
    print Yt4.shape
    utilities.create_submission(linRegT4.getName()+'-T6', IDt4+IDt6 , Yt4)
    logging.info('submission - done')
     
def print_coef(classifier, transformation):
    linRegT4 = utilities.read_pickle('../models/'+classifier+'-'+transformation+'.pkl')
    feat = linRegT4._pipeline.named_steps[transformation].get_feature_names()
    coef = linRegT4._pipeline.named_steps[classifier].coef_
    coef.sort()
    for i, c in enumerate(coef):
        print feat[i], c
        
def gradientBoost_feat_importance(tranformation):
    model = utilities.read_pickle('../models/gradientBoost-'+tranformation+'.pkl')
    feature_importance = model._pipeline.named_steps['gradientBoost'].feature_importances_
    # make importances relative to max importance
    feature_importance = 100.0 * (feature_importance / feature_importance.max())
    sorted_idx = np.argsort(feature_importance)
    pos = np.arange(sorted_idx.shape[0]) + .5
    pl.subplot(1, 2, 2)
    pl.barh(pos, feature_importance[sorted_idx], align='center')
    pl.yticks(pos, model._pipeline.named_steps[tranformation].get_feature_names())
    pl.xlabel('Relative Importance')
    pl.title('Variable Importance')
    pl.show()

if __name__=='__main__':
    #model = RegressionModel('linearRegression', 'T9')
    #model = RegressionModel('rbfSVR', 'T4')
    #model = RegressionModel('randomForrest', 'T8')
    #model = RegressionModel('gradientBoost', 'T7')
    
    #model.train()
    #model.save()
    model = utilities.read_pickle('../models/gradientBoost-T7.pkl')
    #gradientBoost_feat_importance('T9')
    #print_coef('linearRegression','T9')
    model.create_submission()
    
    #model.cross_validation()
    
    #submission_t4_t6()    
    