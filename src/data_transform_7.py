'''
Created on Nov 9, 2012

@author: vanghdi
'''

from collections import Mapping, Sequence
from operator import itemgetter
import utilities
import re
import numpy as np
import scipy.sparse as sp

from pymongo import MongoClient
import sklearn.feature_extraction.text as scikitTxt
from sklearn.base import BaseEstimator, TransformerMixin
from dict_transform import DictVectorizer
from data_transform_4 import DataTranformer4
from userModels import UserUsefulModel

class DataTranformer7(DataTranformer4):
    """
        performs basic data transformations on the yelp data
        using dictionaryVectorizer for on basic input attributes (see xls for details)        
    """
    def __init__(self, sparse):
        
        DataTranformer4.__init__(self, sparse)
        
        self.userUsefulModel = utilities.read_pickle('../models/gradientBoost-user_votes_useful.pkl')
        
        self.ignoreList = ['review_id','type','user_id','business_id','votes', 'text', 'date',
                           'bus_business_id','bus_type','bus_name','bus_full_address','bus_latitude','bus_longitude',
                           'user_type','user_user_id','user_name', 'user_votes','votes']

        #self.add_user_votes = False;
        
    
    def extendX(self, X):
        preprocess = self.build_preprocessor()
        sentTokenizer = self.build_sentence_tokenizer()
        wordTokenizer = self.build_tokenizer()
        client = MongoClient('localhost', 27017)        
        db = client.yelp
            
        for x in X:
            txt = x['text']
            sentences = sentTokenizer(preprocess(txt))
            
            token_pattern = re.compile('[A-Z]{2,}')
            n_w_all_caps= len(token_pattern.findall(txt))
            
            if 'user_votes' in x:
                x['user_votes_useful'] = x['user_votes']['useful']
            else:
                x['user_votes_useful'] = self.userUsefulModel.predict(db, x['review_id'])
            
            if len(txt) > 0:
                n_global_words = 0
                n_global_word_length = 0
                n_s_above_5 = 0
                n_s_above_10 = 0
                n_w_1 = 0 # words up to 1 chars long
                n_w_3 = 0 # words up to 3 chars long
                n_w_5 = 0 # words up to 5 chars long
                n_w_7 = 0 # words up to 7 chars long
                n_w_10 = 0 # words between 5 chars and 10 chars long
                n_w_12 = 0 # words between 5 chars and 10 chars long
                n_w_15 = 0 # words between 10 chars and 15 chars long
                n_w_15plus = 0 # words > 15 chars                 
                for s in sentences:
                    words = wordTokenizer(s)
                    n_words = len(words)
                    n_global_words += n_words
                    if (n_words >= 10):
                        n_s_above_10 += 1
                    elif (n_words >= 5):
                        n_s_above_5 +=1
                    for w in words:
                        word_length = len(w)
                        n_global_word_length += word_length 
                        if word_length > 15:
                            n_w_15plus += 1
                        elif word_length > 12:
                            n_w_15 += 1
                        elif word_length > 10:
                            n_w_12 += 1
                        elif word_length > 7:
                            n_w_10 += 1
                        elif word_length > 5:
                            n_w_7 += 1
                        elif word_length > 3:
                            n_w_5 += 1
                        elif word_length > 1:
                            n_w_3 += 1                        
                        else:
                            n_w_1 += 1
                        
                x[u'n_sentences'] = len(sentences)
                x[u'n_w_all_caps']=n_w_all_caps
                if len(sentences):      
                    x[u'avg_sentence_length'] = n_global_words*1.0 / len(sentences)
                    x[u'perc_sentence_5'] = n_s_above_5*1.0 / len(sentences)
                    x[u'perc_sentence_10'] = n_s_above_10*1.0 / len(sentences)
                    if n_global_words > 0:
                        x[u'n_words'] =  n_global_words
                        x[u'perc_word_all_caps'] = n_w_all_caps / n_global_words
                        x[u'perc_word_1'] = n_w_1*1.0 / n_global_words
                        x[u'perc_word_3'] = n_w_3*1.0 / n_global_words
                        x[u'perc_word_5'] = n_w_5*1.0 / n_global_words
                        x[u'perc_word_7'] = n_w_7*1.0 / n_global_words
                        x[u'perc_word_10'] = n_w_10*1.0 / n_global_words
                        x[u'perc_word_12'] = n_w_12*1.0 / n_global_words
                        x[u'perc_word_15'] = n_w_15*1.0 / n_global_words
                        x[u'perc_word_15plus'] = n_w_15plus*1.0 / n_global_words


     


if __name__ == "__main__":
    import utilities
    import logging, log
    logging.info('train - load data')
    X,Y = utilities.load_training_data()
    logging.info('train - fit model')
    trans = DataTranformer7()
    X=X[:10]
    trans.fit(X) 
    print trans.get_feature_names()
    for x in X:
        print x['user_votes_useful']
    
