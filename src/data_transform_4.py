'''
Created on Nov 9, 2012

@author: vanghdi
'''

from collections import Mapping, Sequence
from operator import itemgetter

import re
import numpy as np
import scipy.sparse as sp

import sklearn.feature_extraction.text as scikitTxt
from sklearn.base import BaseEstimator, TransformerMixin
from dict_transform import DictVectorizer
from data_transform_0 import DataTranformer0


class DataTranformer4(DataTranformer0, scikitTxt.VectorizerMixin):
    """
        performs basic data transformations on the yelp data
        using dictionaryVectorizer for on basic input attributes (see xls for details)        
    """
    def __init__(self, sparse, input='content', charset='utf-8',
                 charset_error='strict', strip_accents=None,
                 lowercase=True, preprocessor=None, tokenizer=None,
                 stop_words=None, token_pattern=ur"(?u)\b\w\w+\b",
                 ngram_range=(1, 1),
                 min_n=None, max_n=None, analyzer='word',
                 max_df=1.0, min_df=2, max_features=None,
                 vocabulary=None, binary=False, dtype=long
                ):
        #print "T4 - sparse: %s" % sparse
        self.dictionaryVectorizer = DictVectorizer(sparse=sparse)
        self.ignoreList = ['review_id','type','user_id','business_id','votes', 'text', 'date',
                           'bus_business_id','bus_type','bus_name','bus_full_address','bus_latitude','bus_longitude',
                           'user_type','user_user_id','user_name', 'user_votes','votes']

        # copied from CountVectorizer
        self.input = input
        self.charset = charset
        self.charset_error = charset_error
        self.strip_accents = strip_accents
        self.preprocessor = preprocessor
        self.tokenizer = tokenizer
        self.analyzer = analyzer
        self.lowercase = lowercase
        self.token_pattern = token_pattern
        self.stop_words = stop_words
        self.max_df = max_df
        self.min_df = min_df
        self.max_features = max_features
        if not (max_n is None) or not (min_n is None):        
            if min_n is None:
                min_n = 1
            if max_n is None:
                max_n = min_n
            ngram_range = (min_n, max_n)
        self.ngram_range = ngram_range
        if vocabulary is not None:
            if not isinstance(vocabulary, Mapping):
                vocabulary = dict((t, i) for i, t in enumerate(vocabulary))
            if not vocabulary:
                raise ValueError("empty vocabulary passed to fit")
            self.fixed_vocabulary = True
            self.vocabulary_ = vocabulary
        else:
            self.fixed_vocabulary = False
        self.binary = binary
        self.dtype = dtype
    
    def build_sentence_tokenizer(self):
        """Return a function that split a string in sequence of sentences"""
        token_pattern = re.compile(ur'([a-z][^\.!?]*[\.!?])')
        return lambda doc: token_pattern.findall(doc)
    
    def extendX(self, X):
        for x in X:
            txt = x['text']
            preprocess = self.build_preprocessor()
            sentTokenizer = self.build_sentence_tokenizer()
            wordTokenizer = self.build_tokenizer()
            sentences = sentTokenizer(preprocess(txt))
            
            token_pattern = re.compile('[A-Z]{2,}')
            n_w_all_caps= len(token_pattern.findall(txt))
            
            if len(txt) > 0:
                n_global_words = 0
                n_global_word_length = 0
                n_s_above_5 = 0
                n_s_above_10 = 0
                n_w_1 = 0 # words up to 1 chars long
                n_w_3 = 0 # words up to 3 chars long
                n_w_5 = 0 # words up to 5 chars long
                n_w_7 = 0 # words up to 7 chars long
                n_w_10 = 0 # words between 5 chars and 10 chars long
                n_w_12 = 0 # words between 5 chars and 10 chars long
                n_w_15 = 0 # words between 10 chars and 15 chars long
                n_w_15plus = 0 # words > 15 chars                 
                for s in sentences:
                    words = wordTokenizer(s)
                    n_words = len(words)
                    n_global_words += n_words
                    if (n_words >= 10):
                        n_s_above_10 += 1
                    elif (n_words >= 5):
                        n_s_above_5 +=1
                    for w in words:
                        word_length = len(w)
                        n_global_word_length += word_length 
                        if word_length > 15:
                            n_w_15plus += 1
                        elif word_length > 12:
                            n_w_15 += 1
                        elif word_length > 10:
                            n_w_12 += 1
                        elif word_length > 7:
                            n_w_10 += 1
                        elif word_length > 5:
                            n_w_7 += 1
                        elif word_length > 3:
                            n_w_5 += 1
                        elif word_length > 1:
                            n_w_3 += 1                        
                        else:
                            n_w_1 += 1
                        
                x[u'n_sentences'] = len(sentences)
                x[u'n_w_all_caps']=n_w_all_caps
                if len(sentences):      
                    x[u'avg_sentence_length'] = n_global_words*1.0 / len(sentences)
                    x[u'perc_sentence_5'] = n_s_above_5*1.0 / len(sentences)
                    x[u'perc_sentence_10'] = n_s_above_10*1.0 / len(sentences)
                    if n_global_words > 0:
                        x[u'n_words'] =  n_global_words
                        x[u'perc_word_all_caps'] = n_w_all_caps / n_global_words
                        x[u'perc_word_1'] = n_w_1*1.0 / n_global_words
                        x[u'perc_word_3'] = n_w_3*1.0 / n_global_words
                        x[u'perc_word_5'] = n_w_5*1.0 / n_global_words
                        x[u'perc_word_7'] = n_w_7*1.0 / n_global_words
                        x[u'perc_word_10'] = n_w_10*1.0 / n_global_words
                        x[u'perc_word_12'] = n_w_12*1.0 / n_global_words
                        x[u'perc_word_15'] = n_w_15*1.0 / n_global_words
                        x[u'perc_word_15plus'] = n_w_15plus*1.0 / n_global_words


    def fit(self, X, y=None):
        """
        Params:
        X : Yelp Review data (List of Dictionaries
        y : (ignored)
        """
        self.extendX(X)
        Xdict = self.filterInputFields(X)
        self.dictionaryVectorizer.fit(Xdict)
        
        return self

    def fit_transform(self, X, y=None):
        """
        """
        self.fit(X)
        return self.transform(X)


    def transform(self, X, y=None):
        """ 
        """
        self.extendX(X)
        Xdict = self.filterInputFields(X)
        return self.dictionaryVectorizer.transform(Xdict, y)    

