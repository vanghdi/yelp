'''
Created on Nov 9, 2012

@author: vanghdi
'''

from collections import Mapping, Sequence
from operator import itemgetter

import numpy as np
import scipy.sparse as sp

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
from dict_transform import DictVectorizer
from data_transform_0 import DataTranformer0


class DataTranformer3(DataTranformer0):
    """
        performs basic data transformations on the yelp data
        using dictionaryVectorizer for on basic input attributes (see xls for details)        
    """
    def __init__(self):
        self.dictionaryVectorizer = DictVectorizer()
        self.ignoreList = ['review_id','type','user_id','business_id','votes', 'text',
                           'bus_business_id','bus_type','bus_name','bus_full_address','bus_latitude','bus_longitude',
                           'user_type','user_user_id','user_name', 'user_votes','votes']

    

