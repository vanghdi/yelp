'''
Created on Nov 9, 2012

@author: vanghdi
'''

from collections import Mapping, Sequence
from operator import itemgetter

import numpy as np
import scipy.sparse as sp

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
from dict_transform import DictVectorizer



class DataTranformer1(BaseEstimator, TransformerMixin):
    """
        performs basic data transformations on the yelp data
        using CountVectorizer on review text and dictionaryVectorizer for other input attributes (see xls for details)        
    """
    def __init__(self):
        self.dictionaryVectorizer = DictVectorizer()
        self.textVectorizer = CountVectorizer()
        self.ignoreList = ['review_id','type','user_id','business_id','votes',
                           'bus_business_id','bus_type','bus_name','bus_full_address','bus_latitude','bus_longitude',
                           'user_type','user_user_id','user_name', 'user_votes','votes',
                           'checkin-time-0','checkin-time-1','checkin-time-2','checkin-time-3','checkin-time-4','checkin-time-5','checkin-time-6','checkin-time-7','checkin-time-8','checkin-time-9','checkin-time-10','checkin-time-11','checkin-time-12','checkin-time-13','checkin-time-14','checkin-time-15','checkin-time-16','checkin-time-17','checkin-time-18','checkin-time-19','checkin-time-20','checkin-time-21','checkin-time-22','checkin-time-23',
                           'checkin-day-0','checkin-day-1','checkin-day-2','checkin-day-3','checkin-day-4','checkin-day-5','checkin-day-6']

    def splitInput(self, X):
        """ split X into dictionary and text input matrices 
        """
        Xtext = [x['text'] for x in X]
        Xdict=[]
        for x in X:
            entry = {}
            for key in x:
                if key not in self.ignoreList+['text']:
                    entry[key]=x[key]
            Xdict.append(entry)
                            
        return Xdict, Xtext

    def fit(self, X, y=None):
        """
        Params:
        X : Yelp Review data (List of Dictionaries
        y : (ignored)
        """
        Xdict, Xtext = self.splitInput(X)
        
        
        self.dictionaryVectorizer.fit(Xdict)
        self.textVectorizer.fit(Xtext)
        
        return self

    def fit_transform(self, X, y=None):
        """
        """
        self.fit(X)
        return self.transform(X)

    def inverse_transform(self, X, dict_type=dict):
        print 'NOT SUPPORTED YET: inverse_transform'
        return None;
    

    def transform(self, X, y=None):
        """ 
        """
        Xdict, Xtext = self.splitInput(X)
        TXdict = self.dictionaryVectorizer.transform(Xdict, y)
        TXtext = self.textVectorizer.transform(Xtext)
          
        return sp.hstack([TXdict, TXtext])

    def get_feature_names(self):
        """Returns a list of feature names
        """
        return self.dictionaryVectorizer.get_feature_names() + self.textVectorizer.get_feature_names()

#     def restrict(self, support, indices=False):
#         
#         """Restrict the features to those in support.
# 
#         Parameters
#         ----------
#         support : array-like
#             Boolean mask or list of indices (as returned by the get_support
#             member of feature selectors).
#         indices : boolean, optional
#             Whether support is a list of indices.
#         """
#         if not indices:
#             support = np.where(support)[0]
# 
#         names = self.feature_names_
#         new_vocab = {}
#         for i in support:
#             new_vocab[names[i]] = len(new_vocab)
# 
#         self.vocabulary_ = new_vocab
#         self.feature_names_ = [f for f, i in sorted(new_vocab.iteritems(),
#                                                     key=itemgetter(1))]
# 
#         return self
