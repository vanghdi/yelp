'''
Created on Nov 9, 2012

@author: vanghdi
'''
from gensim import corpora, models, utils
from nltk.stem.wordnet import WordNetLemmatizer
from gensimLda import getTopicDistribution
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS
from data_transform_4 import DataTranformer4

import logging, log

DICTIONARY_FILE = '../data/yelp/review-1.dict'
CORPUS_FILE = '../data/yelp/review-1.mm'
LDA_FILE = '../data/yelp/review-1.lda'


class DataTranformer5(DataTranformer4):
    """
        extends DataTransformer4 with LDA topic distributions   
    """
    def __init__(self):
        DataTranformer4.__init__(self, sparse=True)
        
        logging.info('DataTranformer5 - init WordNetLemmatizer')
        self.lmtzr = WordNetLemmatizer()
        logging.info('DataTranformer5 - load dictionary (%s)'% DICTIONARY_FILE)
        self.dictionary = corpora.Dictionary.load(DICTIONARY_FILE)
        logging.info('DataTranformer5 - load LDA Model (%s)'% LDA_FILE)
        self.lda = models.ldamodel.LdaModel.load(LDA_FILE)
        
    def tokenize(self, text):
        return [ self.lmtzr.lemmatize(w) for w in utils.tokenize(text, deacc=True, errors='strict', to_lower=True) ]        
        
    def getTopicDistribution(self, text):
        tokens = self.tokenize(text)
        doc = self.dictionary.doc2bow(tokens)
        return self.lda[doc]
        
    
    def addTopicDistribution(self, X):
        #logging.disable(logging.CRITICAL)
        ii = 0
        for x in X:
            txt = x['text']
            topics = self.getTopicDistribution(txt)
            for (i, p) in topics:
                x[u'topic_'+str(i)]=p
            ii+=1
            if ii % 5000 == 0:
                logging.info('DataTranformer5 - addTopicDistribution progress: %d'%ii)
        #logging.basicConfig(level=logging.INFO)
    

    def fit(self, X, y=None):
        """
        Params:
        X : Yelp Review data (List of Dictionaries
        y : (ignored)
        """
        
        # extend first row with dummy topic variables to learn featurenames in self.dictionaryVectorizer.fit(Xdict) 
        # no need to extend all rows
        for i in range(100):
            X[0]['topic_'+str(i)] = 0.0
        DataTranformer4.fit(self, X)
        
        return self

    
    def transform(self, X, y=None):
        """ 
        """
        self.addTopicDistribution(X)
        return super(DataTranformer4, self).transform(X, y) 


    def fit_transform(self, X, y=None):
        """
        """
        self.fit(X)
        return self.transform(X)
    

if __name__ == "__main__":
    import utilities
    import logging, log
    logging.info('train - load data')
    X,Y = utilities.load_training_data()
    logging.info('train - fit model')
    trans = DataTranformer5()
    trans.fit(X) 
    print trans.get_feature_names()
    
