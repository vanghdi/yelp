var app = angular.module('yelpApp', [ 'ngResource' ]).config(
		function($routeProvider) {
			$routeProvider.when('/', {
				controller : MainCtrl,
				templateUrl : 'partials/main.html'
			}).when('/review/:businessId', {
				controller : ReviewCtrl,
				templateUrl : 'partials/review.html'
			}).otherwise({
				redirectTo : '/'
			});
		});


/** NMI Reporter API - resource **/
app.factory('yelpApi', function($resource) {
	return $resource('/nmi-reporter/api/run/yelp/yelp-mongo/:query');
});





