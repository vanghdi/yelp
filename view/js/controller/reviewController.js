

/** MsDbCompareCtrl Controller **/
var ReviewCtrl = function($scope, $routeParams, $log, yelpApi) {
	
	$scope.search = function (){
		
		yelpApi.get({ query: 'review_by_business',  businessId: $routeParams.businessId}, function(res){
			$scope.reviewList = res.data;
			$scope.reviewList.sort(function(a, b) { 
			    return b.stars - a.stars;
			})
			// transformations
			$scope.reviewList.forEach(function(r){
				r.text = r.text.replace(/\n/g, '<br />');
				
			});
			$scope.setGraph($scope.reviewList[0]);
		});		
	}
	
	$scope.getTopTopics = function(r){
		topics=[]
		for (var k in r.topics){
			topics.push({ topic: k.split("_")[1], prob: r.topics[k]});
		};
		topics.sort(function (a, b){
			return b.prob - a.prob;
		});
		topics = topics.slice(0,5);
		topicLabels = [];
		topics.forEach(function(t){
			topicLabels.push((t.prob*100).toFixed(0) +'% - '+ $scope.topics[t.topic]);
		});
		return topicLabels;
		
	}
	
	$scope.setGraph = function(r){

		data = [{ key: 'Topics', color: "#1f77b4", values:[]}];
		for (var k in r.topics){
			data[0].values.push({ label: k, value: r.topics[k]});
		};
		data[0].values.sort(function (a, b){
			return b.value - a.value;
		});
	    
		nv.addGraph(function() {
			  var chart = nv.models.multiBarHorizontalChart()
			      .x(function(d) { return d.label.split("_")[1] })
			      .y(function(d) { return d.value })
			      .margin({top: 30, right: 20, bottom: 50, left: 175})
			      .showValues(false)
			      .tooltips(true)
			      .tooltipContent(function(key, y, e, graph) { return (e*100).toFixed(0) +'% - '+ $scope.topics[y] })
			      .showControls(false);
			
			  chart.yAxis
			      .tickFormat(d3.format(',.2f'));
			
			  d3.select('#chart svg')
			      .datum(data)
			    .transition().duration(500)
			      .call(chart);
			
			  nv.utils.windowResize(chart.update);
			
			  return chart;
		});
	}
	
	$scope.loadTopics = function(){
		$scope.topics = {};
		yelpApi.get({query: 'review_topics'}, function(res){
			res.data.forEach(function(item){
				$scope.topics[item.topic_id] = item.words;
			});			
		});
	};
	
	$scope.loadTopics();	
	
	$scope.offset = 0;
	$scope.limit = 20;
	$scope.search();
};
