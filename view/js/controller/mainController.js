
/** Main Controller **/
var MainCtrl = function($scope, $location, $log, yelpApi) {
	
	$scope.businessSearch = function(category) {
		if (category==undefined){
			yelpApi.get({query: 'business'}, function(res){			
				$scope.businessList = res.data;
			});
		} 
		
	};
	
	
	$scope.businessSearch();
};
