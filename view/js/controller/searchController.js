



/** Search Controller **/
var SearchCtrl = function($scope, $location, executionApi) {
	
	$scope.searchListing = function (){
		
		$scope.rowList = [];
		var opusQuery = { 	connection: 'crawler02-test', 
				query: 'opus_publication', 
				p: 'listingid:'+$scope.listingId 
			};
		var cbProcessedQuery = { 	connection: 'crawler02-test', 
				query: 'cb_processed', 
				p: 'listingid:'+$scope.listingId 
			};
		var cbUnProcessedQuery = { 	connection: 'crawler02-test', 
				query: 'cb_unprocessed', 
				p: 'listingid:'+$scope.listingId 
			};
		executionApi.get(opusQuery, function (res){
			$scope.rowList = $scope.rowList.concat(res.data);
			
		});
		executionApi.get(cbProcessedQuery, function (res){
			$scope.rowList = $scope.rowList.concat(res.data);
			
		});
		executionApi.get(cbUnProcessedQuery, function (res){
			$scope.rowList = $scope.rowList.concat(res.data);
			
		});
		
	};
	
	$scope.rowList = [];
};
