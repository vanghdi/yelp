

mongoser:
	cd <...>/lib/mongser/
	./bin/mongoser.sh -fg start  (runs process in foreground)    
	or 
	./bin/mongoser.sh start
	
	
chrome:
	(necessary because browser will not allow calls from tomcat to mongoser (running on different port)
    open -a Google\ Chrome --args --disable-web-security
    	